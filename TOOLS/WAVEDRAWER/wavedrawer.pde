
////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

int		LENGTH		= 2048;
float	SAMPLES[] 	= new float[LENGTH];

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

void			init_wave() {
	int			i;

	for (i = 0; i < LENGTH; ++i) {
		SAMPLES[i] = sin(map(i, 0, LENGTH, 0, TWO_PI));
	}
}

void			draw_wave(int x, int y, int w, int h) {
	int			i;
	float		px, py;
	float		index, value;
	float		x_from, x_to;
	float		y_from, y_to;

	strokeWeight(1);
	noFill();
	/// DRAW AXIS
	stroke(255, 255, 0);
	line(x, y + h * 0.5, x + w, y + h * 0.5);
	for (i = 0; i <= 10; ++i) {
		px = map(i, 0, 10, x, x + w);
		py = y + h * 0.5;
		line(px, py - 10, px, py + 10);
	}
	/// DRAW CURSOR
	stroke(255, 0, 255);
	if (mouseX >= x && mouseX < x + w) {
		for (py = y + (millis() * 0.1) % 10; py < y + h; py += 10)
			line(mouseX, py, mouseX, py + 5);
	}
	/// DRAW WAVE
	stroke(255);
	beginShape();
	for (i = 0; i < LENGTH; ++i) {
		px = map(i, 0, LENGTH, x, x + w);
		py = map(SAMPLES[i], -1, 1, y + h, y);
		vertex(px, py);
	}
	endShape();
	/// DRAW ZONE
	stroke(0, 255, 255);
	rect(x, y, w, h);
	/// HANDLE EVENTS
	if (mousePressed == true
	&& mouseX >= x && mouseX < x + w
	&& mouseY >= y && mouseY < y + h) {
		x_from = floor(map(pmouseX, x, x + w, 0, SAMPLES.length));
		x_to = floor(map(mouseX, x, x + w, 0, SAMPLES.length));
		y_from = map(pmouseY, y, y + h, 1, -1);
		y_to = map(mouseY, y, y + h, 1, -1);
		px = x_from;
		while (px != x_to) {
			py = map(px, x_from, x_to, y_from, y_to);
			if (px >= 0 && px < SAMPLES.length)
				SAMPLES[(int)px] = py;
			px += (px < x_to) ? 1 : -1;
		}
	}
}

//////////////////////////////////////////////////
/// MAIN
//////////////////////////////////////////////////

void		keyPressed() {
	if (key == ' ') {
		save_wave("out", SAMPLES, 32);
		println("[FILE SAVED]");
	}
}

void		setup() {
	size(1200, 800);
	init_wave();
}

void		draw() {

	background(0);
	draw_wave(50, 50, width - 100, height - 100);
}
