
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

void			save_table(String path, float samples[]) {
	byte		data[];
	ByteBuffer	buffer;
	int			buffer_size;
	long		sample;
	int			i;

	/// COMPUTE BUFFER SIZE
	buffer_size = samples.length * 2;
	buffer = ByteBuffer.allocate(buffer_size);
	buffer.order(ByteOrder.LITTLE_ENDIAN);
	/// FILL BUFFER CONTENT
	for (i = 0; i < samples.length; ++i) {
		sample = (long)(samples[i] * 65535);
		buffer.putShort((short)sample);
	}
	/// SAVE FILE
	data = buffer.array();
	saveBytes(path + ".suntable", data);
}

void			save_wave(String path, float samples[], int bits_per_sample) {
	int			bytes_per_sample;
	byte		data[], data24[];
	ByteBuffer	buffer, buffer24;
	int			buffer_size;
	long		sample;
	int			i;

	buffer24 = ByteBuffer.allocate(4);
	buffer24.order(ByteOrder.LITTLE_ENDIAN);
	bytes_per_sample = bits_per_sample / 8;
	/// COMPUTE BUFFER SIZE
	buffer_size = samples.length * bytes_per_sample + 44;
	buffer = ByteBuffer.allocate(buffer_size);
	/// FILL BUFFER HEADER
	//// CHUNK ID			char[4]			4
	buffer.order(ByteOrder.BIG_ENDIAN);
	buffer.put(byte('R'));
	buffer.put(byte('I'));
	buffer.put(byte('F'));
	buffer.put(byte('F'));
	//// CHUNK SIZE			int				8
	buffer.order(ByteOrder.LITTLE_ENDIAN);
	buffer.putInt(4 + 8 + 16 + 8 + samples.length * bits_per_sample);
	//// FORMAT				char[4]			12
	buffer.order(ByteOrder.BIG_ENDIAN);
	buffer.put(byte('W'));
	buffer.put(byte('A'));
	buffer.put(byte('V'));
	buffer.put(byte('E'));
	//// SUB CHUNK 1 ID		char[4]			16
	buffer.put(byte('f'));
	buffer.put(byte('m'));
	buffer.put(byte('t'));
	buffer.put(byte(' '));
	//// SUB CHUNK 1 SIZE	int				20
	buffer.order(ByteOrder.LITTLE_ENDIAN);
	buffer.putInt(16);
	//// AUDIO FORMAT		short			22
	if (bits_per_sample < 32)
		buffer.putShort((short)1);			// WAVE_FORMAT_PCM
	else
		buffer.putShort((short)3);			// WAVE_FORMAT_IEEE_FLOAT
	//// NUM CHANNELS		short			24
	buffer.putShort((short)1);
	//// SAMPLE RATE		int				28
	buffer.putInt(48000);
	//// BYTE RATE			int				32
	buffer.putInt(48000 * 1 * bytes_per_sample);
	//// BLOCK ALIGN		short			34
	buffer.putShort((short)(1 * bytes_per_sample));
	//// BITS PER SAMPLE	short			36
	buffer.putShort((short)bits_per_sample);
	//// SUB CHUNK 2 ID		char[4]			40
	buffer.order(ByteOrder.BIG_ENDIAN);
	buffer.put(byte('d'));
	buffer.put(byte('a'));
	buffer.put(byte('t'));
	buffer.put(byte('a'));
	//// SUB CHUNK 2 SIZE	int				44
	buffer.order(ByteOrder.LITTLE_ENDIAN);
	buffer.putInt(samples.length * bytes_per_sample);
	/// FILL BUFFER CONTENT
	for (i = 0; i < samples.length; ++i) {
		/// 8bits (unsigned)
		if (bits_per_sample == 8) {
			sample = (long)((samples[i] + 1) * 127);
			buffer.put((byte)sample);
		/// 16bits (signed)
		} else if (bits_per_sample == 16) {
			sample = (long)(samples[i] * 32767);
			buffer.putShort((short)sample);
		/// 24bits (signed)
		} else if (bits_per_sample == 24) {
			sample = (long)(samples[i] * 8388607);
			buffer24.putInt(0, (int)sample);
			data24 = buffer24.array();
			buffer.put(data24[0]);
			buffer.put(data24[1]);
			buffer.put(data24[2]);
		/// 32bits (floating point)
		} else if (bits_per_sample == 32) {
			buffer.putFloat(samples[i]);
		}
	}
	/// SAVE FILE
	data = buffer.array();
	saveBytes(path + ".wav", data);
}
