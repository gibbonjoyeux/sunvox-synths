
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

int		LENGTH		= 2048;
float	SAMPLES[] 	= new float[LENGTH * 3];

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// MAIN
//////////////////////////////////////////////////

void		setup() {
	int		i, j;
	float	sample;

	/// SIN
	for (i = 0; i < LENGTH; ++i) {
		sample = sin(((float)i / (float)LENGTH) * TWO_PI);
		SAMPLES[i] = sample;
	}
	/// TRI
	for (i = 0; i < LENGTH; ++i) {
		if (i < LENGTH / 2)
			sample = (float)i / (float)(LENGTH / 2);
		else
			sample = 1.0 - (float)(i - (LENGTH) / 2) / (float)(LENGTH / 2);
		sample = 2.0 * sample - 1.0;
		SAMPLES[LENGTH + i] = sample;
	}
	/// SAW
	for (i = 0; i < LENGTH; ++i) {
		sample = 2.0 * ((float)i / (float)LENGTH) - 1.0;
		SAMPLES[LENGTH * 2 + i] = sample;
	}
	/// SAVE
	save_wave("wavetable", SAMPLES, 32);
	exit();
}
