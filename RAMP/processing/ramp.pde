
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

int		RAMP_STEPS	= 6;
int		LENGTH		= 256;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

void		setup() {
	float	[]samples;
	int		i, j;
	int		step, step_half;
	int		index;
	float	level;

	samples = new float[LENGTH];
	step = (int)((float)LENGTH / ((float)RAMP_STEPS / 2.0));
	step_half = step / 2;
	/// MAKE WAVES
	for (i = 0; i < RAMP_STEPS; ++i) {
		/// CLEAR WAVE
		for (j = 0; j < LENGTH; ++j)
			samples[j] = -1;
		/// FILL WAVE
		for (j = 0; j < step; ++j) {
			/// GO UP
			if (j < step_half)
				level = (float)j / (float)(step_half);
			/// GO DOWN
			else
				level = 1.0 - ((float)(j - step_half) / (float)(step_half));
			/// COMPUTE SAMPLE INDEX
			index = (step_half * (i - 1) + j) % LENGTH;
			if (index < 0)
				index = LENGTH + index;
			/// SET SAMPLE
			samples[index] = level;
		}
		/// SAVE WAVE
		save_table("ramp_" + (i + 1), samples);
	}
	exit();
}
